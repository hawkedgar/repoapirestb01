package com.santana.rest;

import com.santana.rest.utils.BadSeparator;
import com.santana.rest.utils.EstadosPedido;
import com.santana.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto, empleadosController.getCadena(origen,"."));
    }
    @Test
    public void testSeparadorGetCadena(){
        try{
            //Se envía el separador correcto o incorrecto
            Utilidades.getCadena("Edgar Santana", " ");
            fail("Se esperaba BadSeparator");
        }catch (BadSeparator bs){

        }
    }

    @Test
    public void testGetAuthor(){
        assertEquals("Edgar Santana", empleadosController.getAppAutor());
    }

    //Anotacion que realiza una prueba con parámetros
    @ParameterizedTest
    @ValueSource(ints = {1,3,5,7,-13,Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }
    @ParameterizedTest
    @ValueSource(strings = {" ","",})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ","\t","\r"})
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }
    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        //En caso de que haya más elementos en la enumeración de los permitidos en la condición de la función
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }


}
