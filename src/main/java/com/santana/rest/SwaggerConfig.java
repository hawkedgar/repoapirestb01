package com.santana.rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

//Las URL's para probar la documentación son:
//localhost:8080/api/v2/api-docs
//localhost:8080/api/swagger-ui.html

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    //El tipo de documentación a devolver es Swagger 2
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.santana.rest" + ""))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }
    private ApiInfo getApiInfo(){
        return new ApiInfo(
                "The rest emp api", "Devuelve información de empleados", "1.0","http://codmind.com/terms",
                new Contact("Demo", "https://demo.com", "apis@demo.com"), "LICENSE", "LICENSE URL",
                Collections.emptyList()
                );
    }
}
