package com.santana.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpBackApplication.class, args);
	}

}
