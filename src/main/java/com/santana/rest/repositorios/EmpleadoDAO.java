package com.santana.rest.repositorios;

import com.santana.rest.empleados.Capacitacion;
import com.santana.rest.empleados.Empleado;
import com.santana.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


//Indicamos a través de la anotación que es un repositorio
@Repository
public class EmpleadoDAO {
    //Generar de logs, asociados a la clase
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    //Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    //Se crea un constructor estático que se ejecuta como primer paso dentro de la clase
    static {
        Capacitacion capa1 = new Capacitacion("2020/03/12","DBA");
        Capacitacion capa2 = new Capacitacion("2020/02/12","Back End");
        Capacitacion capa3 = new Capacitacion("2020/01/12","Front End");
        Capacitacion capa4 = new Capacitacion("2019/08/12","Insurgente");
        ArrayList<Capacitacion> una = new ArrayList<Capacitacion>();
        una.add(capa1);
        ArrayList<Capacitacion> dos = new ArrayList<Capacitacion>();
        dos.add(capa1);
        dos.add(capa2);
        ArrayList<Capacitacion> tres = new ArrayList<Capacitacion>();
        tres.addAll(dos);
        tres.add(capa3);
        ArrayList<Capacitacion> todas = new ArrayList<Capacitacion>();
        todas.add(capa4);
        todas.addAll(tres);
        list.getListaEmpleados().add(new Empleado(1,"Antonio", "Lopez", "antonio@lopez.com",una));
        list.getListaEmpleados().add(new Empleado(2,"Vicente", "Guerrero", "vicente@guerrero.com",dos));
        list.getListaEmpleados().add(new Empleado(3,"Josefa", "Ortiz", "josefa@ortiz.com",tres));
        list.getListaEmpleados().add(new Empleado(4,"Ignacio", "Allende", "ignacio@allende.com",todas));
    }
    //Función para devolver todos los Empleados
    public Empleados getAllEmpleados(){
        logger.debug("Empleados devueltos");
        return list;
    }
    //Función para devolver un solo empleado
    public Empleado getEmpleado(int id){
        for (Empleado emp : list.getListaEmpleados()){
            if (emp.getId() == id){
                return emp;
            }
        }
        //En caso de haber barrido el arreglo y no encontrar el dato buscado se devuelve null
        return null;
    }
    //Función pública para añadir un objeto al arreglo list
    public void addEmpleado(Empleado emp){
        list.getListaEmpleados().add(emp);
    }
    //Función para actualizar un objeto del arreglo list, tomando como entrada todos los datos
    public void updEmpleado(Empleado emp){
        Empleado current = getEmpleado(emp.getId());
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    //Función para actualizar un objeto del arreglo list, tomando como parametros de entrada el id y los datos
    //modificados
    public void updEmpleado(int id, Empleado emp){
        Empleado current = getEmpleado(id);
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    //Función para eliminar un elemento y recibe como entrada el id del objeto
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if(current == null) return "NO";
        Iterator it = list.getListaEmpleados().iterator();
        //hasNext, identifica si hay un siguiente elemento en el arreglo
        while(it.hasNext()){
            //El elemento del arreglo se convierte en un objeto de tipo empleado
            Empleado emp = (Empleado) it.next();
            if(emp.getId() == id){
                it.remove();
                break;
            }
        }
        return "OK";
    }
    //Función para hacer un update de un campo específico, de acuerdo a los datos de entrada
    public void softUpdEmpleado(int id, Map<String, Object> updates){
        Empleado current = getEmpleado(id);
        //Devuelve todos los elementos del mapa en forma de array
        for(Map.Entry<String, Object> update: updates.entrySet()){
            switch(update.getKey()){
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;
            }
        }
    }
    //Función que devuelve las capacitaciones de un empleado
    public List<Capacitacion> getCapacitacionEmpleado(int id){
        Empleado current = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>();
        if (current != null) caps = current.getCapacitaciones();
        return caps;
    }
    //Función que añade una capacitación a un empleado
    public Boolean addCapacitacion(int id, Capacitacion cap){
        Empleado current = getEmpleado(id);
        if (current == null) return false;
        current.getCapacitaciones().add(cap);
        return true;
    }
}
