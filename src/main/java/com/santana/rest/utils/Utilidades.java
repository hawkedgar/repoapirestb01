package com.santana.rest.utils;

public class Utilidades {
    public static String getCadena(String texto, String separador) throws BadSeparator{
        if(separador.length() > 1 || (separador.equals(" "))) throw new BadSeparator();
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        for (char letra : letras) {
            //Itera a través de cada uno de los caracteres
            //En caso de que cada caracter es distinto a espacio, se monta la letra + el separador
            if (letra != ' ') {
                resultado += letra + separador;
            } else {
                //En caso de que sea un espacio en blanco, se respeta dicho espacio y no se agrega separador
                if (!resultado.trim().equals("")) {
                    resultado = resultado.substring(0, resultado.length() -1);
                }
                resultado += letra;
            }
        }
        //Se elimina el último separador de la cadena
        if (resultado.endsWith(separador)) {
            resultado = resultado.substring(0, resultado.length() -1);
        }
        return resultado;
    }
    //Función que obtiene un valor y decide si es impar o no
    public static boolean esImpar(int number){
        return (number % 2 != 0);
    }
    public static boolean estaBlanco(String texto){
        return texto == null || texto.trim().isEmpty();
    }
    //Funcion que cuenta el total de estados y en caso de haber más de el numero de elementos que están dentro
    //de la condición, regresa false
    public static boolean valorarEstadoPedido(EstadosPedido ep){
        int valor = ep.ordinal();
        return (valor >= 0 && valor <= 4);
    }
}
