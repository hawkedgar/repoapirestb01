package com.santana.rest.utils;

public enum EstadosPedido {
    ACEPTADO,
    COCINANDO,
    ENTREGA,
    ENTREGADO,
    VALORADO,
    ENVIANDO;
}
