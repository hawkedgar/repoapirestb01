package com.santana.rest;

import com.santana.rest.empleados.Capacitacion;
import com.santana.rest.empleados.Empleado;
import com.santana.rest.empleados.Empleados;
import com.santana.rest.repositorios.EmpleadoDAO;
import com.santana.rest.utils.Configuracion;
import com.santana.rest.utils.EstadosPedido;
import com.santana.rest.utils.Utilidades;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;


@RestController
//Controlador principal
@RequestMapping(path = "/empleados")
public class EmpleadosController {
    @Autowired
    private EmpleadoDAO empDAO;
    //Devuelve la lista de todos los empleados
    @GetMapping(path = "/")
    public Empleados getEmpleados(){
        return empDAO.getAllEmpleados();
    }
    //ResponseEntity devuelve el código de retorno conforme al estándar de HTTP
    //Devolver un solo elemento del arreglo, conforme al id
    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empDAO.getEmpleado(id);
        if(emp == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok(emp);
        }
//        return empDAO.getEmpleado(id);
    }
    //Función para añadir un elemento a la lista
    @PostMapping("/")
    //Se deposita en la variable emp, el nuevo objeto de Empleado
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        //Asignar un id consecutivo
        Integer id = empDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empDAO.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
    //Función para actualizar un elemento de la lista, recibiendo un parámetro, todo el objeto
    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        empDAO.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }
    //Función para actualizar un elemento, recibiendo dos parámetros
    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp){
        empDAO.updEmpleado(id, emp);
        return ResponseEntity.ok().build();
    }
    //Función para eliminar un elemento
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoXId(@PathVariable int id){
        String resp = empDAO.deleteEmpleado(id);
        if(resp == "OK"){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }
    }
    //Patch para enviar solo alguno de los elementos del objeto, indicando que campos se quieren modificar
    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softUpdEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates){
        empDAO.softUpdEmpleado(id, updates);
        return ResponseEntity.ok().build();

    }
    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id){
        return ResponseEntity.ok().body(empDAO.getCapacitacionEmpleado(id));
    }
    //Alta de una capacitacion
    @PostMapping(path = "/{id}/capacitaciones", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addCapacitacionEmp(@PathVariable int id, @RequestBody Capacitacion cap){
        if(empDAO.addCapacitacion(id, cap)){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.notFound().build();
        }

    }
    @Value("${app.titulo}") private String titulo;
    @GetMapping("/titulo")
    public String getAppTitulo(){
        String modo = configuracion.getModo();
        return String.format("%s (%s)", titulo, modo);
    }
    @Autowired
    private Environment env;
    @GetMapping("/autor")
    public String getAppAutor(){
        return configuracion.getAutor();
        //return env.getProperty("app.autor");
    }
    @Autowired
    Configuracion configuracion;

    @GetMapping("/cadena")
    public String getCadena (@RequestParam String texto, @RequestParam String separador){
        try {
            return Utilidades.getCadena(texto, separador);
        }catch(Exception e){
            return "";
        }

    }

}
